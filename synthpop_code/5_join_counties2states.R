##########################################################################
# ---------------- 4.1 Sample Consumption Expenditures ------------------
##########################################################################
args <- commandArgs(TRUE)
seed <- as.integer(args[1])

# Load Packages
library(tidyverse)
library(dplyr)
library(simPop)
library(ranger)
library(surveysd)
library(here)
library(rmarkdown)
library(readxl)
library(laeken)
library(systemfit)
library(hablar)
library(sampleSelection)
set.seed(seed)


# Load Census Data
communities_p=readRDS(here("workingfiles/communities_p.rds"))
counties_p=readRDS(here("workingfiles/counties_p.rds"))




for (k in 1:16) {
	print(k)
	synthpop_D = readRDS(paste0("results/synthpop_elas_",counties_p$AGS_12[1],"_",seed,".rds")  ) %>%
		select(reg_name,rid,county,state,reg_class,hid,npers,dem_gr,decile,aeq,edu,ses,nace,worktime,building.age,space,
          	heating.system,heating.energy,owner,car.owner,inc.labor,
          	inc.capital,inc.transfer,inc.gross,ded.taxes,ded.socialsec,inc.disp,s.y_n,y1,
          	s.electricity,s.food,s.heating,s.housing,s.fuel,s.other) 

	synthpop_D = as.data.table(synthpop_D[-c(1:nrow(synthpop_D)),])

	for (l in counties_p$AGS_12[counties_p$RS_Land == k]) {
  		if (file.exists(paste0("results/synthpop_elas_",l,"_",seed,".rds"))){
    			print(l)
    		    synthpop_raw = readRDS(paste0("results/synthpop_elas_",l,"_",seed,".rds")) %>%
		select(reg_name,rid,county,state,reg_class,hid,npers,dem_gr,decile,aeq,edu,ses,nace,worktime,building.age,space,
          	heating.system,heating.energy,owner,car.owner,inc.labor,
          	inc.capital,inc.transfer,inc.gross,ded.taxes,ded.socialsec,inc.disp,s.y_n,y1,
          	s.electricity,s.food,s.heating,s.housing,s.fuel,s.other) 

    
     		    synthpop = as.data.table(synthpop_raw[,colnames(synthpop_D)])
    		    synthpop_D=rbind(synthpop_D,synthpop) 
		}  
  	}
saveRDS(synthpop_D,paste0("results/synthpop_elas_",k,"_",seed,".rds"))
}



